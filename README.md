# Python : Découverte

> Découverte du langage de programmation Python

## Objectifs

- Aborder les grands concept de programmation : instructions, variables, structures de contrôles, structures de données
- Commencer à comprendre que certaines tâches de traitement de données sont plus simples avec un langage de programmation complet

## Partie 1 : installation

Avant de pouvoir commencer à explorer le langage Python, il est nécessaire
d'installer notre environnement :

- [ ] Installer l'interpréteur Python 3 :
```bash
sudo apt install python3
```
- [ ] Installer l'environnement Jupyter :
```bash
pip3 install jupyter
```
- [ ] Tester que les programmes `python3` et `jupyter notebook` fonctionnent

## Partie 2 : notions de programmation

Ouvrir, compléter et exécuter le notebook Jupyter [`notions.ipynb`](notions.ipynb).

## Partie 3 : fichiers

Ouvrir, compléter et exécuter le notebook Jupyter [`fichiers.ipynb`](fichiers.ipynb).

## Partie 4 : expressions régulières

Ouvrir, compléter et exécuter le notebook Jupyter [`regex.ipynb`](regex.ipynb).
